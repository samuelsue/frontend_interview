# HTML5语义化标签

+ `<title>`页面主题内容
+ `<header>` 页眉信息，比如导航栏，搜索框
+ `<nav>`导航栏
+ `<article><section>`文档内容、区段
+ `<aside>` 侧栏，页边广告等
+ `<footer>`页脚
+ `<small><em><strong>`

# CSS

## 字体

引用自定义字体

```css
@font-face{
	font-family: '自定义字体';
	src: url('Sansation_Light.ttf'),
     	url('Sansation_Light.eot'); /* IE9 */
}
.content {
    font-family: '自定义字体'
}
```

关于行高(line-height)，在一个块级元素`<div>`中包含多个行内元素`<span>`或者行内块元素，`<div>`的高度由内部元素**最高**的行内元素或行内块元素**撑开**，内部元素的水平对齐方式`vertical-align`默认是`baseline`，若想居中对其，则应改为`middle`

## 边框

1. 边框可以是背景图

```css
.box{
    width: 600px;
    height: 400px;
    border: 30px solid transparent;
    border-image: url("../../img/border.png") 30 round; // round是整数重复
}
```

边框图为:

![](E:\webworkspace\frontend_interview\img\border.png)

每个方块的大小是30px，因此可以实现格子边框效果

2. 三角形

原理: 边框衔接的部分是一个斜切的，所以可以将某个边的边框颜色透明，即可拼出三角形

```css
.triangle{
    width: 0px;
    height: 0px;
    border-bottom: 100px solid red;
    border-right: 100px solid transparent;
    border-left: 100px solid transparent;
  }
```

## 文本换行

```css
/*
overflow-wrap: normal | break-word;  正常情况下，超过边框的文本单词不换行，break-word单词也会换行
word-break: normal | keep-all | break-all | break-word ; 所有文本不换行 | 所有文本都换行 | 只有单词换行
white-space: normal | nowrap;  nowrap实现长文本不换行
*/
```

## 浮动

+ 浮动元素会尽量靠左(右)和上布局
+ 浮动元素脱离文档流，但是不会脱离文本流(非浮动元素的文字内容不会被浮动元素盖住)
+ html文档的解析是从上往下的，加入遇到了左浮动+块元素+右浮动，右浮动元素会另起一行，因为先解析到块级元素占满了一行，解决方案是写成: 左浮动+右浮动+块元素(三栏布局)
+ 父级元素清除浮动

## inline-block

+ 像文本一样排列block元素
+ 需要处理间隙(因为是以文本处理block元素，文本之间是有缝隙的)，父级元素设置`font-size:0`，inline-block元素各自设置字体大小

## clip-path

对对象进行裁剪

```css
/* Keyword values */
clip-path: none;

/* <clip-source> values */ 
clip-path: url(resources.svg#c1);

/* <geometry-box> values */
clip-path: margin-box;
clip-path: border-box;
clip-path: padding-box;
clip-path: content-box;
clip-path: fill-box;
clip-path: stroke-box;
clip-path: view-box;

/* <basic-shape> values */
clip-path: inset(100px 50px);
clip-path: circle(50px at 0 100px);
clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);
clip-path: path('M0.5,1 C0.5,1,0,0.7,0,0.3 A0.25,0.25,1,1,1,0.5,0.3 A0.25,0.25,1,1,1,1,0.3 C1,0.7,0.5,1,0.5,1 Z');

/* Box and shape values combined */
clip-path: padding-box circle(50px at 0 100px);

/* Global values */
clip-path: inherit;
clip-path: initial;
clip-path: unset;
```

可以做动画(比如缩放动画，鼠标移动到头像上放大)