// 父类
function SuperType(name) {
    this.name = name;
    this.property = [ 'red', 'yellow', 'blue' ];
    this.getName = function () {
        return this.name
    }
}

// 子类
function SubType(name) {
    // 父类构造函数传参
    SuperType.call(this,name);

    // 子类自身属性
    this.age = 20;
}

var sub = new SubType("Samuel");
console.log(sub.getName());
sub.property.push("black");

var sub2 = new SubType("Zhang");
console.log(sub2.property);
