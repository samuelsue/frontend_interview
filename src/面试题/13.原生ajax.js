// get
function getAjax(url) {
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : ActiveXObject("microsoft.XMLhttp");
    // 请求成功后的回调
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4&&xhr.httpRequestStatusCode === 200){
            console.log(xhr.responseText);
        }
    }
    xhr.open('get',url,true); // 最后一个参数是 是否异步
    xhr.send();
}

// post
function postAjax(url) {
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : ActiveXObject("microsoft.XMLhttp");
    // 请求成功后的回调
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4&&xhr.httpRequestStatusCode === 200){
            console.log(xhr.responseText);
        }
    }
    xhr.open('post',url);
    xhr.setRequestHeader('content-type','application/x-www-form-urlencoded'); // 请求头
    xhr.send("username=Samuel"); // 请求参数
}
