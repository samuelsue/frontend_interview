// 原型式继承
function object(obj) {
    function F() {
    }

    F.prototype = obj;
    return new F();
}

// 继承父类方法
function inheritPrototype(SubType, SuperType) {
    var prototype = object(SuperType.prototype);
    prototype.constructor = SubType;
    SubType.prototype = prototype;
}

// 父类构造方法
function SuperType(name) {
    this.name = name;
    this.colors = ['red', 'green'];
}

// 父类方法
SuperType.prototype.getName = function () {
    return this.name;
}

// 子类构造方法
function SubType(name, age) {
    SuperType.call(this, name);  // 调用一次父类构造方法
    this.age = age; // 子类属性
}

// 子类继承父类的方法
inheritPrototype(SubType, SuperType);

// 子类方法
SuperType.prototype.say = function () {
    return this.name + ":" + this.age;

}


