function SuperType(name) {
    this.name = name;
    this.colors = ['red','blue','green'];
}

SuperType.prototype.sayName = function () {
    return this.name
}

function SubType(name, age) {
    SuperType.call(this, name);
    this.age = age;
}

SubType.prototype = new SuperType();
SubType.prototype.constructor = SubType; // 子类原型的构造器指向子类
SubType.prototype.sayAge = function () {
    return this.age
}
