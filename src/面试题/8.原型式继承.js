var person = {
    name: 'Samuel',
    age: 24,
    wifes: ['ZhangBoy']
}

// 原型式继承
function object(obj) {
    function F() {}
    F.prototype = obj;
    return new F();
}

var sub = object(person);
sub.wifes.push('ZhangDaxian');

var sub2 = object(person);
console.log(sub2.wifes); // 子类实例共享父类引用.

// ES5还有个方法是Object.create, 实现功能跟上面那个类似。
var sub3 = Object.create(person);
console.log(sub3.wifes);
