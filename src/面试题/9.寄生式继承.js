function object(o) {
    function F() {
    }

    F.prototype = o;
    return new F();
}

// 借助一个函数来增强子类的属性和方法
function createAnother(o) {
    var clone = object(o);
    clone.sayHi = function () {
        console.log('Hi');
    }
    return clone;
}

var person = {
    name: 'Tom',
    friends: ['Shelby', 'Court', 'Van']
};

var anotherPerson = createAnother(person);
anotherPerson.sayHi();                              // "Hi"
anotherPerson.friends.push('Rob');
console.log(anotherPerson.friends);              // ['Shelby', 'Court', 'Van', 'Rob']
var yerAnotherPerson = createAnother(person);
console.log(yerAnotherPerson.friends);
