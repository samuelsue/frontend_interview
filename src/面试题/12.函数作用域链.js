var scope = "global";

function fn1() {
    return scope;
}

function fn2() {
    return scope;
}

fn1();
fn2();


// 闭包的作用域链
function outer() {
    var scope = "outer";

    function inner() {
        return scope;
    }

    return inner;
}

var fn = outer();
fn();
