// 父类
function SuperType() {
    this.property = true;
}

SuperType.prototype.getProperty = function () {
    return this.property
}

// 子类
function SubType() {
    this.subType = false;
}
SubType.prototype = new SuperType();  // 子类的prototype指向父类引用
SubType.prototype.getSubProperty = function () {
    return this.subType
}

let instance = new SubType();
console.log(instance);
console.log(instance.getProperty());
console.log(instance instanceof SuperType);
console.log(instance instanceof Object);
