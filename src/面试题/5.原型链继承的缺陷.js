function SuperType() {
    this.property = ['red','yellow','blue'];
}
function SubType() {}
SubType.prototype = new SuperType();

// 创建子类实例时，无法给父类构造函数传参
var sub1 = new SubType();
console.log(sub1.property); // [ 'red', 'yellow', 'blue' ]

// 来自原型对象的引用属性时所有实例共享的
sub1.property.push('pink');
var sub2 = new SubType();
console.log(sub2.property); // [ 'red', 'yellow', 'blue', 'pink' ]
