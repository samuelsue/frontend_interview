# 1. 什么是SVN

SVN全称SubVersion

特点:

+ 操作简单，入门容易
+ 支持跨平台
+ 支持版本回退

属于C/S结构软件

# 2. 基本操作

## 服务端

### 1. 安装服务端

安装[visualsvn server](https://www.visualsvn.com/server/)

基本一路下一步, 可以手动改一改仓库(repository)路径

### 2. 创建仓库

```bash
svnadmin create [代码仓库目录路径]
```

创建成功后会出现以下文件

![image8a6d4d36c98281e6.png](https://wx2.sbimg.cn/2020/06/05/image8a6d4d36c98281e6.png)

### 3. 服务器监管

```bash
svnserve -d -r [代码仓库路径]
# -d 表示后台运行
# -r 表示监管的是一个目录
```

这样就可以通过`svn://localhost`访问到相关仓库

### 4. 权限控制

默认情况下，匿名用户无法读取仓库，因此要配置权限

```bash
# 打开仓库\conf\svnserve.conf文件 去掉anon-access=read的注释
anon-access = read
# 也可以改成write(可读可写)
```

这时要重启svn服务器，重新启动服务器监管

## 客户端

### 1. 客户端安装

[tortoisesvn](https://tortoisesvn.net/downloads.zh.html)

### 2. 连接&checkout

进入你的本地工程目录,右键选择

![svn-client.png](https://www.z4a.net/images/2020/06/05/svn-client.png)

输入svn地址`svn://[ip]`，可以选择要checkout的repository，确定，然后再右键选择svn checkout 确认，即可，此时该目录下出现一个隐藏目录`.svn`

# 3. 详细使用

## 1. 三大指令

### checkout

检出操作，连接到svn服务端，更新服务端数据到本地

注意:

> checkout只在第一次连接时操作一次，以后请使用update

### commit

在本地工程目录有修改操作后，可以右键选择`svn commit`弹出TortoiseSNV提交窗口.

### update

更新仓库到本地

## 2. 图标集

1.常规图标：

![image](https://img-blog.csdn.net/20180328103246502?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：当客户端文件与服务器文件完全同步的时候，系统就会显示以上图标

2.冲突图标：

![](https://img-blog.csdn.net/20180328103403163?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：当客户端文件与服务器数据存在冲突时，系统会出现以上图标

3.删除图标：

![](https://img-blog.csdn.net/20180328103716160?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：当服务器端的文件已经删除时候，那么客户端文件就会显示以上图标

4.增加图标：

![](https://img-blog.csdn.net/20180328104731765?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：我们编写的文件已经提交到队列中，系统会显示以上图标

5.无版本图标：

![](https://img-blog.csdn.net/20180328104759181?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：我们编写的文件没有提交到队列中，系统会显示以上图标

6.修改图标：

![](https://img-blog.csdn.net/20180328104819950?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：客户端文件已经修改但是未提交，系统会出现以上图标

7.只读图标：

![](https://img-blog.csdn.net/20180328104838760?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：当客户端文件以只读的形式存在时，系统会出现以上图标

8.锁定图标：

![](https://img-blog.csdn.net/20180328104913393?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：当服务器数据已经锁定的时候，客户端文件会自动显示锁定图标

9.忽略图标：

![](https://img-blog.csdn.net/20180328104939591?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


含义：客户端文件已经忽略，不需要进行提交上传，系统会显示忽略图标。

忽略功能的实现：点击需要忽略的文件右击 TortoiseSVN 

![](https://img-blog.csdn.net/20180328110430671?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzYzNTQwMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

刷新一下即可。

## 3. 版本回退

![4e88d31d396578d4810e56327a7ff299.png](https://wx1.sbimg.cn/2020/06/06/4e88d31d396578d4810e56327a7ff299.png)

![9d86402404e93e63af185a530058cbef.png](https://wx2.sbimg.cn/2020/06/06/9d86402404e93e63af185a530058cbef.png)

[![6c87cd4c0ba44cc560bcf86614727d96.png](https://www.z4a.net/images/2020/06/06/6c87cd4c0ba44cc560bcf86614727d96.png)](https://www.z4a.net/image/Tu57bC)

## 4. 版本冲突

当多个人同时修改一个文件，commit时会出现冲突

解决办法：

1. 先更新(update)服务器数据到本地

[![fa11646fa4a1dd41e2f8468e7fa251df.png](https://wx1.sbimg.cn/2020/06/06/fa11646fa4a1dd41e2f8468e7fa251df.png)](https://sbimg.cn/image/kY9BJ)

2. 一般情况下，查看并修改整合后的文件确认后，把其他文件删除
3. 提交

## 5. 配置多仓库和权限控制

如何监管多个仓库?

直接监管所有仓库的父目录(所以仓库都在同一个文件夹下面)

然后就可以通过不同的url来访问不同的仓库

```
svn://localhost/shop
svn://localhost/web
```

权限控制

1. 编辑conf/svnserve.conf

   ```
   password-db = passwd
   authz-db = authz
   ```

2. 编辑认证用户名和密码conf/passwd

   ```shell
   [users]
   # harry = harryssecret
   # sally = sallyssecret
   admin = password
   sam = sys963
   ```

3. 编写授权文件conf/authz

   ```shell
   # 分组
   [groups]
   # harry_and_sally = harry,sally
   # harry_sally_and_joe = harry,sally,&joe
   admin = admin,sam  # 组名 = 用户,...
   
   # 仓库权限
   [Shop:/]  # 仓库路径
   @admin = rw  # 用户组权限
   * = r # 匿名用户权限
   ```

# 4. 使用svnserver结合IDE

管理用户

![92b226f11bee216817dea2c2d84b7a8a.png](https://www.z4a.net/images/2020/06/06/92b226f11bee216817dea2c2d84b7a8a.png)

可以自行添加删除用户

新建仓库:

![aac41f9d1d5c3e187b4acb48ae926cdb.png](https://www.z4a.net/images/2020/06/06/aac41f9d1d5c3e187b4acb48ae926cdb.png)

基本上一路下一步，除了权限配置需要自定义

![5beb82cf37c8efd8fcd2a656d10f8147.png](https://www.z4a.net/images/2020/06/06/5beb82cf37c8efd8fcd2a656d10f8147.png)

配置完成后，会自动生成https的url，访问即可查看当前仓库

结合IDE使用(以webstorm为例)

![19435388879ae5f14d750fc018dfd998.png](https://www.z4a.net/images/2020/06/06/19435388879ae5f14d750fc018dfd998.png)

![aeda0d21082b22bbb31a1a80d3c9f00f.png](https://www.z4a.net/images/2020/06/06/aeda0d21082b22bbb31a1a80d3c9f00f.png)

然后一路OK就行了，在Webstorm环境下自动checkout后会打开工作窗口，在右上角分别存在快捷命令

![8fd2b1a2501843b4b78156a137a8b921.png](https://www.z4a.net/images/2020/06/06/8fd2b1a2501843b4b78156a137a8b921.png)

依次为update,commit，还有查看提交历史，点击提交历史的功能，即可显示所有commit的记录，在对应记录上右键Get即可切换版本(版本回退)

![af14bc93efdb5a0a8e5b8a1309538212.png](https://www.z4a.net/images/2020/06/06/af14bc93efdb5a0a8e5b8a1309538212.png)