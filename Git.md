# 1. Git和SVN

Git是分布式的，SVN是集中式

Git复杂概念比较多，SVN简单易上手

Git分支廉价，SVN分支昂贵

# 2. Git文件状态

![](https://git-scm.com/book/en/v2/book/02-git-basics/images/lifecycle.png)



- **Untracked**: 未跟踪, 此文件在文件夹中, 但并没有加入到git库, 不参与版本控制. 通过`git add` 状态变为`Staged`
- **Unmodify**: 文件已经入库, 未修改, 即版本库中的文件快照内容与文件夹中完全一致. 这种类型的文件有两种去处, 如果它被修改, 而变为`Modified`. 如果使用`git rm`移出版本库, 则成为`Untracked`文件
- **Modified**: 文件已修改, 仅仅是修改, 并没有进行其他的操作. 这个文件也有两个去处, 通过`git add`可进入暂存`staged`状态, 使用`git checkout` 则丢弃修改过, 返回到`unmodify`状态, 这个`git checkout`即从库中取出文件, 覆盖当前修改
- **Staged**: 暂存状态. 执行`git commit`则将修改同步到库中, 这时库中的文件和本地文件又变为一致, 文件为`Unmodify`状态. 执行`git reset HEAD filename`取消暂存, 文件状态为`Modified`

# 3. 常用命令

## git add

将文件添加到暂存区

`git add .`比较常用，注意要配合`.gitignore`文件来忽略不需要的文件(夹)，如node_modules,.idea, .vscode

> 如果你已经push了突然想起来要搞.gitignore怎么办

```bash
git rm -r --cached .
git add .
git commit -m 'update .gitignore'
```

## git commit

提交命令, `git commit -m "修改信息"`

## git log

查看当前日志，即所有的提交记录

## git reset

在commit之前，你发现有文件你不想进行版本控制(或者本次不想更新)

`git reset [文件名]`

**版本回退**

`git reset <commitID> --hard`  commitID通过`git log`查看

+ --hard: 强制回退，不管你修改了啥
+ --soft: 保留更改，且变更的内容处于staged
+ --mixed: 保留更改，变更内容处于Modified

## 设置alias简化命令

`安装目录/gitconfig`可以自行设置alias

```bash
[alias]
	ad = add .
	cmm = commit -m
```

## git reflog

查看所有的日志(假如版本回退之后又后悔了，想回到最新的版本，log查不到最新的id)

## git chekcout

分支相关操作:

1. 创建新的分支

   `git checkout -b <name> <template>`以template分支为模板创建name分支，`template`可以不填(默认为master)

2. 切换分支

   `git checkout <name>`

## git branch

查看本地分支信息

删除本地已经合并`git branch –d 分支名`

删除本地未合并的分支`git branch –D 分支名`

远程分支删除`git push origin -d 分支`

## git push

将本地仓库(分支)推送到远程仓库

```bash
git push --set-upstream origin <分支名> # 将远程的分支作为本地分支的上流分支
git push  # 将本地仓库推送到远程
git push -f # 强制使用本地仓库覆盖远程仓库
```

## git fetch

查看远程仓库信息，获取远程分支信息，更新代码

注意，此时`git branch`是不能显示远程的分支信息，但是可以切换到远程分支`git checkout <远程分支>`

如果想在远程分支的基础上切换新的分支`git checkout -b <本地分支> origin <远程分支>`

## git pull

pull = fetch + merge

## git rebase

加入在master分支上和bc分支上有不同的commit，需要按照更新时间来排列所有的commit

![be432ef933a027b8ab686762cb8d7778.png](https://www.z4a.net/images/2020/06/06/be432ef933a027b8ab686762cb8d7778.png)

```bash
# 在bc分支上执行rebase
git rebase master
# 正常情况下会出现冲突，需要手动处理
git add .
# 继续下一个commit结点
git rebase --continue
# 继续处理冲突， 再add . 在继续--continue....
```

## git merge

开发分支（dev）上的代码达到上线的标准后，要合并到 master 分支

```bash
    git checkout dev
    git pull
    git checkout master
    git merge dev
    git push -u origin master
```

