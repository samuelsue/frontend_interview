# Vue.js

[TOC]

https://cn.vuejs.org/

# 安装

Vue.js是一个前端JS框架, 设计模式采用MVVM, 对比MVC的模式, 传统的MVC(Model, View, Controler)如果要更新View, 需要通过Controler和Model交互, 然后获取信息后传递个View, 而MVVM(Model, View, ViewModel) 将VIew更加抽象化, 使其能通过ViewModel和Model交互.

前端页面引入

```html
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```



# 指令

基本使用:

```html
<!--html结构-->
<div id="app">
  {{ message }}
</div>
```

```javascript
var app = new Vue({
  el: '#app',  // 指定根节点
  data: {
    message: 'Hello Vue!'  // 数据
  }
})
```

默认页面上将呈现'Hello Vue', 如果此时在浏览器的控制台中修改`app.message`, 页面上的内容也将同步修改.

## v-bind

常用于操作标签属性, 有几个特殊属性有别的写法, 一般v-bind操作**class或style**

例如:

```html
<div id="app" v-bind:class="cls_str"></div>

<script>
    var app = new Vue({
        el: '#app',
        data: {
            cls_str: 'active'
        }
    });
</script>
```

上述代码中, 最终呈现的结果是`class='active'`, 同样可以同步修改app.cls_str

还可以支持这样写来操作大部分的属性(**v-bind可以省略**)

```html
<div id="app" :[cls_name]="cls_str">
        {{message}}
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue',
            cls_name: 'class',  // 键
            cls_str: 'active'   // 值
        }
    });
</script>
```



## v-model

实现数据双向绑定, 即数据(data)和input之间的双向绑定. **只能用在表单元素中**

```html
<div id="app">
    <input type="text" v-model="message"><br>
    <span>{{message}}</span>
</div>

<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: "hello"
        }
    })
</script>
```

input因为有v-model绑定了app.message, 因此修改input的value, span中的message也随之改变, 注意**v-model进来的数据都是字符串**

简单的做一个页面加法器:

```html
<body>
    <div id="div1">
        <input type="text" v-model="n1">+
        <input type="text" v-model="n2">=
        {{sum()}}
    </div>
</body>

<script>
    let vm=new Vue({
        el: '#div1',
        data: {
            n1: 0, n2: 0
        },
        methods: {  // methods里面写函数
            sum(){
                return parseInt(this.n1)+parseInt(this.n2);
            }
        }
    });
</script>
```

## v-text / v-html

v-text设置innerText(不解释html标签, 直接打印), v-html设置innerHtml(解释html标签)

```html
<div id="div1">
    <div v-text="str">
    </div>
</div>

<script>
    let vm=new Vue({
        el: '#div1',
        data: {
            str: `<p>11月26日，一段题为“张家口经开区国土局局长接访时玩手机”的视频在网络平台流传。</p>
<p>11月26日，一段题为“张家口经开区国土局局长接访时玩手机”的视频在网络平台流传。</p>
<p>11月26日，一段题为“张家口经开区国土局局长接访时玩手机”的视频在网络平台流传。</p>`
        }
    });
</script>
```

v-text结果包含`<p>`的, v-html是解释了p标签, 出现3行文本.

## v-on

绑定事件监听器

```html
<div id="div1">
    {{a}}
    <input type="button" value="+1" v-on:click="fn(5)">
</div>

<script>
    let vm=new Vue({
        el: '#div1',
        data: {
            a: 12
        },
        methods: {
            fn(n){
                this.a+=n;// this指向的是app, this.XX可以访问data里面的数据
            }
        }
    });
</script>
```

点击button, 即可将a+5, `v-on:click="fn"`也可以写, 但是最好还是加上括号, 方便传参.

同时, v-on还可以简写: `@click="fn(5)"`是监听click事件, 还可以`@keydown`

### 事件修饰符

+ `.stop`: `@click.stop="fn()"` 阻止事件的冒泡
+ `.prevent`: 阻止默认事件, 可用于表单
+ `.self`: 当事件是从侦听器绑定的元素本身触发时才触发回调。
+ `.{keyCode | keyAlias}`: 特定按键触发, 比如`@keydown.enter.e="fn()"`, 表示按下ctrl+e时触发回调
+ `.native` : 监听原生事件
+ `.once`: 事件只触发一次回调
+ `.capture`: 捕获模式回调, 就是修改冒泡的顺序, 让`.capture`的时间优先冒泡, 然后再往里面传递, 再往`.capture`外部传递.
+ `.left/.right/middle`: 鼠标左键/右键/中键



## v-show&v-if&v-else

v-if: 根据表达式的值的真假条件渲染元素。在切换时元素及它的数据绑定 / 组件**被销毁并重建**。

v-show: 根据表达式之真假值，切换元素的 `display` CSS 属性。

```javascript
<div id="app">
    <div class="if" v-if="flag">v-if</div>
    <div class="show" v-show="flag">v-show</div>
    <button @click="isshow()">show/hide</button>
</div>
<script>
    let app = new Vue({
        el: "#app",
        data: {
            flag: true,
        },
        methods: {
            isshow(){
                this.flag = !this.flag;
            }
        }
    })
</script>
```

最终效果为: 

![074d7f84480d234885669c8aeb3e0d2f.png](https://www.z4a.net/images/2019/05/29/074d7f84480d234885669c8aeb3e0d2f.png)

v-if的标签被销毁(替换为注释), v-show的元素被display隐藏.

v-else是配合v-if使用的: 即当v-if='表达式'为真则渲染v-if的标签, 否则渲染v-else的标签

```html
<table v-if="items.length>0">
    
</table>
<p v-else>
    元素个数为0
</p>
```





## v-for

1.数组    v-for="item,index in array"
2.json    v-for="val,key in json"
3.字符串  v-for="char,index in str"
4.数字    v-for="i in num"  (循环创建元素)

```javascript
<div id="el">
    <ul>
    	<li v-for="user in users">用户名:{{user.name}}, 密码:{{user.password}}</li>
    </ul>
</div>
<script>
        let app = new Vue({
            el: "#el",
            data: {
                users: [
                    {name: 'blue', password: '123456'},
                    {name: 'zhangsan', password: '654321'},
                    {name: 'lisi', password: '111111'},
                ]
            }
        });
</script>
```

## v-pre

**跳过这个元素和它的子元素的编译过程**。可以用来显示原始 Mustache 标签。跳过大量没有指令的节点会加快编译。  可以提高性能, 防止意外.

貌似作用就是不解析这个标签, 直接转义成字符串输出(就是{{user}}这样)

## v-cloak

这个指令保持在元素上直到关联实例结束编译, 就是说, 在刚开始编译的时候, 有些元素不需要显示(因为显示出来的效果和编译后的不对), 因此使用这个指令,在刚开始编译时, 把元素隐藏, 编译完再显示.

```css
[v-cloak] {display:none}  /* 配合css使用 */ 
```

```html
<div v-cloak>
  {{ message }}
</div>
```

`{{message}}`在编译完成之前不会显示.

## 自定义指令

```javascript
// 全局自定义指令
Vue.directive('focus',{
    // 指令绑定到el上时, el是一个原生JS对象, 此时还未插入到html DOM中
    bind: function(el){

    },
    // el元素插入到html DOM中
    inserted: function(el){
        el.focus();
    },
    // el 更新时
    update: function(el){

    }
});
```

```html
<!-- 自定义的指令需要以v-XXX格式 -->
<input type="text" v-model="searchStr" v-focus>
```



bind, inserted, update钩子函数的参数表:

`el`：指令所绑定的元素，可以用来直接操作 DOM 。

`binding`：一个对象,就是你在指令传递的参数，包含以下属性：

- `name`：指令名，不包括 `v-` 前缀。
- `value`：指令的绑定值，例如：`v-my-directive="1 + 1"` 中，绑定值为 `2`。
- `oldValue`：指令绑定的前一个值，仅在 `update` 和 `componentUpdated` 钩子中可用。无论值是否改变都可用。
- `expression`：字符串形式的指令表达式。例如 `v-my-directive="1 + 1"` 中，表达式为 `"1 + 1"`。
- `arg`：传给指令的参数，可选。例如 `v-my-directive:foo` 中，参数为 `"foo"`。
- `modifiers`：一个包含修饰符的对象。例如：`v-my-directive.foo.bar` 中，修饰符对象为 `{ foo: true, bar: true }`。

**简写bind和update, 合为一个函数**

```javascript
Vue.directive('color-swatch', function (el, binding) {
  el.style.backgroundColor = binding.value
})
```



# 计算属性

Vue的设计初衷是不希望模板中放入太多逻辑, 这样会使得模板过重且难以维护.

```html
<div id="example">
  {{ message.split('').reverse().join('') }}
</div>
```

这样的写法很不友好, 因此我们需要对复杂的逻辑进行封装, 使用**计算属性**(意思就是把一个方法当做属性来使用, 语法有点像python的@property)

```html
<div id="app">
    <input type="text" name="firstName" v-model="firstName">
    <input type="text" name="lastName" v-model="lastName">
    <div>{{sayHello}}</div>
</div>
<script>
    let app = new Vue({
        el: "#app",
        data: {
            firstName: "",
            lastName: ""
        },
        methods: {
            tellMeNow(){
                return Date.now();
            }
        },
        computed: {
            sayHello: function () {
                return "Hello to" + " " + this.lastName + " " + this.firstName;
            },
        }
    });
</sciprt>
```



# 过滤器

在模板中可以使用过滤器, 不过貌似Vue中没有做过滤器的封装, 因此, 需要自定义过滤器

```javascript
// Vue.filter(fileterName, function(data,arg1,...))  data是过滤器接收的数据
```

```javascript
// 案例, 时间格式化的过滤器 (全局过滤器), 不需要在Vue中注册
let datefilter = Vue.filter('dateFormat',function(dataStr){
    let date = new Date(dataStr);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    
    return `${year}-${month}-${day}`;
})
```

```html
<td>{{new Date() | dataformat}}</td>
```

私有过滤器:

```javascript
new Vue({
    // ...
    filters:{
        dateFormat: function(data, arg1,...){
            
        }
    }
})
```







# 监听

虽然计算属性在大多数情况下更合适，但有时也需要一个自定义的侦听器。这就是为什么 Vue 通过 `watch` 选项提供了一个更通用的方法，来响应数据的变化。**当需要在数据变化时执行异步或开销较大的操作时**，这个方式是最有用的。

```html
<div id="app">
    <input type="text" name="hello" v-model="user.info">
</div>
<script>
    let app = new Vue({
        el: '#app',
        data: {
            user: {
                info: ""
            }
        },
        watch: {
            "user.info": function (newValue,oldValue) {
                console.log("info变成了" + this.user.info);
            }
        }
    })
</script>
```

基本用途: 在用户在搜索栏输入数据时, 进行数据索引的搜索(请求索引数据等), 然后显示.



# VueRouter

https://router.vuejs.org/zh/

Vue Router 是 [Vue.js](http://cn.vuejs.org)[ ](http://cn.vuejs.org) 官方的路由管理器。它和 Vue.js 的核心深度集成，让构建单页面应用变得易如反掌

`<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>`

基本的结构:

```html
<body>
    <div id="div1">
        <div class="links">
            <!--<router-link class="nav" to="/news/58">页面1</router-link>-->
            <router-link class="nav" :to="{name: 'news', params: {id: 98}}">页面1</router-link>
            <router-link class="nav" to="/b">页面2</router-link>
            <router-link class="nav" to="/c">页面3</router-link>
        </div>
        文字
        <!--1.路由容器, 组件component渲染的地方-->
        <router-view></router-view>
    </div>
</body>

<script>
    //2.路由表
    let router=new VueRouter({
        routes: [
            {
                path: '/news/:id/',
                name: 'news',
                component: {
                    template: '<div>新闻：{{$route.params.id}}</div>'
                }
            },
            {
                path: '/b',
                name: 'user',
                component: {template: '<div>bbbbb</div>'}
            },
            {
                path: '/c',
                name: 'index',
                component: {template: '<div>cccccc</div>'}
            }
        ]
    });

    let vm=new Vue({
        el: '#div1',
        data: {},
        router   // 将router传入.
    });
</script>
```



## 命名路由

```javascript
 {
     path: '/news/:id/',   // 可以捕获参数
     name: 'news',        // 路由的名称
     component: {
         template: '<div>新闻：{{$route.params.id}}</div>'  // $route.param获取所有参数
     }
 },
```

传参:

```html
<router-link :to="{name:'user', params:{id:98}}">页面b路由</router-link>
<!--或者是-->
<router-link to="/news/98">页面b路由</router-link>
```

或者通过methods动态添加路由(跳转):`this.$router.push(urlpath)`以及`this.$router.replace({name:xxx, params:{id: xxx}})`

```html
.......
<div class="links">
    <input type="button" value="页面1" @click="fn1()">
    <input type="button" value="页面2" @click="fn2()">
    <input type="button" value="页面3" @click="fn3()">
</div>
.......
<script>
    ...
    methods: {
        fn1(){
            //this.$router.push('/news/19');
            this.$router.replace({name: 'news', params: {id: Math.random()}});
        },
        fn2(){
            this.$router.push('/b');
        },
        fn3(){
            this.$router.push('/c');
        }
    },
</script>
```



## 路由高亮active

默认的高亮class为:`.router-link-active`, 也可以自定义高亮类

```javascript
new VueRouter({
    routes:[
        //...
    ],
    linkActiveClass: 'myactive'  // 自定义高亮Class
})
```



## 路由监听

Vue的watch可以监听路由的状态变化, 即当前路由和即将跳转的下一步路由

```javascript
watch: {
    // 注意是$route, 不是#router
    $route(value, old_value){
        console.log(value, old_value);
    }
}
```


## 命名视图

`<route-view>`可以携带一个属性name, 即为视图命名, 此后在视图渲染时,可以根据名称到对应视图容器中渲染

```html
<router-view name="header"></router-view>
<router-view></router-view>
```

视图渲染:

```javascript
let Header = {
    template: `
        <div class="nav">
          <router-link class="nav-item" to="/index">首页</router-link>
          <router-link class="nav-item" to="/news">新闻</router-link>
        </div>
	`
};
let Home = {
    template: `
        <div>
        首页
        </div>
        `
}
//....
routes: [
    {
      path: '/index',
      name: 'index',
      components: {
        header: Header,  // 在router-view[name="header"]中渲染
        default: Home   // 在没有命名的router-view渲染
      }
    }
]
```

## 路由嵌套

```javascript
// ... route 
{
    path: '/news',    // 父级路由
    name: 'news',
    components: {
        header: Header,
        default: News
    },
    children: [		// 子路由
        {
            path: 'port',
            name:"port",
            component: {
                template: `<div>port</div>`   // 子路由的要渲染的内容.
            }
        }
    ]
}

let News = {
    template: `
    <div>新闻1
    <router-link :to="{name:'port'}">go port</router-link>
    <router-view></router-view>                // 子路由容器
    </div>
    `
}
```

## 重定向与别名

重定向：

```javascript
// 重定向方式一
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: '/b' }
  ]
})
// 重定向方式二
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: { name: 'foo' }}
  ]
})
```

别名：就是地址不变，但是显示别名指向的组件

```javascript
const router = new VueRouter({
  routes: [
    { path: '/a', component: A, alias: '/b' }
  ]
})
```



## 路由组件传参

```javascript
const User = {
  props: ['id'],
  template: '<div>User {{ id }}</div>'
}
const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User, props: true },

    // 对于包含命名视图的路由，你必须分别为每个命名视图添加 `props` 选项：
    {
      path: '/user/:id',
      components: { default: User, sidebar: Sidebar },
      props: { default: true, sidebar: false }
    }
  ]
})
```

`props:true`表示该组件会捕获url参数，可以通过组件内部的props注册的属性直接使用，false表示不通过prop传递该参数

+ Bool模式：如果 `props` 被设置为 `true`，`route.params` 将会被设置为组件属性

+ 对象模式

  ```javascript
  const router = new VueRouter({
    routes: [
      { path: '/promotion/from-newsletter', component: Promotion, props: { newsletterPopup: false } }
    ]
  })
  // 会向组件Promotion传递一个newsletterPopup:false的参数
  ```

+ 函数模式

  ```javascript
  const router = new VueRouter({
    routes: [
      { path: '/search', component: SearchUser, props: (route) => ({ query: route.query.q }) }
    ]
  })
  ```

  URL `/search?q=vue` 会将 `{query: 'vue'}` 作为属性传递给 `SearchUser` 组件。

  

## 路由守卫

路由独享的守卫:

- `beforeRouteEnter`在渲染该组件的对应路由被 confirm 前调用,  不！能！获取组件实例 `this`
- `beforeRouteUpdate` 在当前路由改变，但是该组件被复用时调用,举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，可以访问组件实例 `this`
- `beforeRouteLeave`导航离开该组件的对应路由时调用,可以访问组件实例 `this`

实例:

```html
<router-link to="/a/123a">页面a路由</router-link>
<router-link to="/a/13a">页面a路由参数变化</router-link>

...
<script>
routes: [
    {
        path: '/a/:id/',  // 参数捕获
        name: 'news',    //命名路由
        component: {
            template: '<div>这是a路由,参数:{{$route.params.id}}</div>',
            // 路由守卫
            beforeRouteUpdate(to, from, next){
                console.log(to, from, next);
                if(true==confirm("确定要离开此页面么?")){
                    next();
                }
            }
        }
    },
]
</script>
```

## 路由元信息

```javascript
 {
     path:"bar",
     component: Bar,
     meta: {
         // 可以携带一些元信息， 在路由守卫中(如router.beforeEach((to,from,next)))进行获取判断
         requireAuth: true
     }
 }
```

一个路由匹配到的所有路由记录会暴露为 `$route` 对象 (还有在导航守卫中的路由对象) 的 `$route.matched` 数组。因此，我们需要遍历 `$route.matched` 来检查路由记录中的 `meta` 字段。例如:

```javascript
router.beforeEach((to,from,next)=>{
    console.log(to);
    if(to.matched.some(record => record.meta.requireAuth)){ // 元信息
        if(!auth.loggedIn()){
            next({
                path:"/login",
                query: {
                    redirect: to.fullPath
                }
            })
        }else{
            next();
        }
    }else{
        next();
    }
})
```

## 过渡动效

类似Vue的动画，使用transition

```html
<transition>
  <router-view></router-view>
</transition>
```

还可以动态设置不同的transition的name属性达到不同的动画属性

```html
<transition mode="out-in" :name="transitionName">
    <router-view></router-view>
</transition>
```

```javascript
let vm = new Vue({
    data() {
        return {
            transitionName: 'move-left'
        }
    },
    el: '#app',
    router,
    watch: {
        '$route'(to, from) {
            // 通过监听来实现切换动画类名
            console.log(to.path);
            this.transitionName = to.path=='/foo'?'move-left':'move-right';
        }
    }
})
```







# 网络通信

## axios

http://www.axios-js.com/zh-cn/docs/

### get

```javascript
import Vue from "vue/dist/vue.esm";
import Axios from 'axios';

let vm = new Vue({
    el: '#div1',
    data: {
        name: "",
        age: "",
        loaded: false
    },
    async created() {
        try {
            let {data} = await Axios.get("./data/user.json"); //get请求
            this.name = data.name;
            this.age = data.age;

            this.loaded = true;
        } catch (e) {
            alert("加载数据失败,请刷新");
        }
    },
    template: `
    <div v-if="loaded">
    <label>名字：</label><span>{{name}}</span><br/>
    <label>年龄：</label><span>{{age}}</span>
  </div>
    `
})
```

发送携带参数:

```javascript
// 注意跨域问题
let resp = await Axios.get("http://localhost:8080/test", {
    params: {aaa: 11, bbb: 12}
});
this.result2 = resp.data;
```

### post

```javascript
import Vue from 'vue/dist/vue.esm';
import Axios from 'axios';
import {stringify} from 'querystring';  //使用

// Axios通用配置
const axios = Axios.create({
    transformRequest: [  // 转换请求
        function (data) {
            let result = stringify(data);
            console.log(result);
            return result;
        }
    ]
});
// post请求
let {data} = await axios({
    url: 'http://localhost:8080/upload',  // 注意跨域, 后端需允许跨域
    method: 'post',
    data: {a: 55, b: 99}    // 这里注意Axios会自作主张按照JSON格式发送, 所以需要之前配置转换请求
});
```

### 所有配置信息

```javascript
{
   // `url` 是用于请求的服务器 URL
  url: '/user',

  // `method` 是创建请求时使用的方法
  method: 'get', // default

  // `baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL。
  // 它可以通过设置一个 `baseURL` 便于为 axios 实例的方法传递相对 URL
  baseURL: 'https://some-domain.com/api/',

  // `transformRequest` 允许在向服务器发送前，修改请求数据
  // 只能用在 'PUT', 'POST' 和 'PATCH' 这几个请求方法
  // 后面数组中的函数必须返回一个字符串，或 ArrayBuffer，或 Stream
  transformRequest: [function (data, headers) {
    // 对 data 进行任意转换处理
    return data;
  }],

  // `transformResponse` 在传递给 then/catch 前，允许修改响应数据
  transformResponse: [function (data) {
    // 对 data 进行任意转换处理
    return data;
  }],

  // `headers` 是即将被发送的自定义请求头
  headers: {'X-Requested-With': 'XMLHttpRequest'},

  // `params` 是即将与请求一起发送的 URL 参数
  // 必须是一个无格式对象(plain object)或 URLSearchParams 对象
  params: {
    ID: 12345
  },

   // `paramsSerializer` 是一个负责 `params` 序列化的函数
  // (e.g. https://www.npmjs.com/package/qs, http://api.jquery.com/jquery.param/)
  paramsSerializer: function(params) {
    return Qs.stringify(params, {arrayFormat: 'brackets'})
  },

  // `data` 是作为请求主体被发送的数据
  // 只适用于这些请求方法 'PUT', 'POST', 和 'PATCH'
  // 在没有设置 `transformRequest` 时，必须是以下类型之一：
  // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
  // - 浏览器专属：FormData, File, Blob
  // - Node 专属： Stream
  data: {
    firstName: 'Fred'
  },

  // `timeout` 指定请求超时的毫秒数(0 表示无超时时间)
  // 如果请求话费了超过 `timeout` 的时间，请求将被中断
  timeout: 1000,

   // `withCredentials` 表示跨域请求时是否需要使用凭证
  withCredentials: false, // default

  // `adapter` 允许自定义处理请求，以使测试更轻松
  // 返回一个 promise 并应用一个有效的响应 (查阅 [response docs](#response-api)).
  adapter: function (config) {
    /* ... */
  },

 // `auth` 表示应该使用 HTTP 基础验证，并提供凭据
  // 这将设置一个 `Authorization` 头，覆写掉现有的任意使用 `headers` 设置的自定义 `Authorization`头
  auth: {
    username: 'janedoe',
    password: 's00pers3cret'
  },

   // `responseType` 表示服务器响应的数据类型，可以是 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
  responseType: 'json', // default

  // `responseEncoding` indicates encoding to use for decoding responses
  // Note: Ignored for `responseType` of 'stream' or client-side requests
  responseEncoding: 'utf8', // default

   // `xsrfCookieName` 是用作 xsrf token 的值的cookie的名称
  xsrfCookieName: 'XSRF-TOKEN', // default

  // `xsrfHeaderName` is the name of the http header that carries the xsrf token value
  xsrfHeaderName: 'X-XSRF-TOKEN', // default

   // `onUploadProgress` 允许为上传处理进度事件
  onUploadProgress: function (progressEvent) {
    // Do whatever you want with the native progress event
  },

  // `onDownloadProgress` 允许为下载处理进度事件
  onDownloadProgress: function (progressEvent) {
    // 对原生进度事件的处理
  },

   // `maxContentLength` 定义允许的响应内容的最大尺寸
  maxContentLength: 2000,

  // `validateStatus` 定义对于给定的HTTP 响应状态码是 resolve 或 reject  promise 。如果 `validateStatus` 返回 `true` (或者设置为 `null` 或 `undefined`)，promise 将被 resolve; 否则，promise 将被 rejecte
  validateStatus: function (status) {
    return status >= 200 && status < 300; // default
  },

  // `maxRedirects` 定义在 node.js 中 follow 的最大重定向数目
  // 如果设置为0，将不会 follow 任何重定向
  maxRedirects: 5, // default

  // `socketPath` defines a UNIX Socket to be used in node.js.
  // e.g. '/var/run/docker.sock' to send requests to the docker daemon.
  // Only either `socketPath` or `proxy` can be specified.
  // If both are specified, `socketPath` is used.
  socketPath: null, // default

  // `httpAgent` 和 `httpsAgent` 分别在 node.js 中用于定义在执行 http 和 https 时使用的自定义代理。允许像这样配置选项：
  // `keepAlive` 默认没有启用
  httpAgent: new http.Agent({ keepAlive: true }),
  httpsAgent: new https.Agent({ keepAlive: true }),

  // 'proxy' 定义代理服务器的主机名称和端口
  // `auth` 表示 HTTP 基础验证应当用于连接代理，并提供凭据
  // 这将会设置一个 `Proxy-Authorization` 头，覆写掉已有的通过使用 `header` 设置的自定义 `Proxy-Authorization` 头。
  proxy: {
    host: '127.0.0.1',
    port: 9000,
    auth: {
      username: 'mikeymike',
      password: 'rapunz3l'
    }
  },

  // `cancelToken` 指定用于取消请求的 cancel token
  // （查看后面的 Cancellation 这节了解更多）
  cancelToken: new CancelToken(function (cancel) {
  })
}
```

## fetch

### get请求

```javascript
let res=await fetch('data/user.json');
let data=await res.json();  // 记住这里也要await

this.name=data.name;
this.age=data.age;
```

### post请求

```javascript
import Vue from 'vue/dist/vue.esm';

let vm=new Vue({
    el: '#div1',
    data: {
       result: "waiting"
    },
    methods: {
        async fn_submit(){
            let form = this.$refs['form1'];
            let formData = new FormData(form);
            let formdata = new FormData(form);

            let res = await fetch(form.action, {
                method: form.method,
                body: formData,
                mode: 'cors',  // 跨域声明
            });
            let result = await res.text();  // 这里要await
            this.result = result;
        }
    },
    template:
    `
      <div>
        <form ref="form1" action="http://localhost:8080/upload" method="post">
          <input type="text" name="a" value="0" />
          <input type="text" name="b" value="0" />
          <input type="submit" value="计算"  @click.prevent="fn_submit()"/>
        </form>
        {{result}}
      </div>
    `
})
```



# 组件

组件是可以复用的Vue实例, 并且带有名字. 

## props传参

```javascript
// cmp1.js   组件js
import Vue from "vue/dist/vue.esm";

export default Vue.component('cmp1', {
    props: ["name", "list"],   // 通过properties(标签属性)接受的参数
    data() {			// 这里data必须是一个函数, 因为组件复用时使用各自独立的数据
        return {a: 77, b: 55};
    },
    template: `
    <div>
    姓名: {{name}}<br>
    result= {{a+b}}<br>
    <ul>
        <li v-for="item in list">{{item}}</li>
    </ul>
    </div>
    `
});


// =============================================
// vm.js
import Vue from "vue/dist/vue.esm";
import "./cmp1";

let app = new Vue({
    el: "#app",
    template:
    `<div>
        <cmp1 name="Samuel" :list="[11,22,33]">    // 这就是给组件传参
    </div>`
});
```

注意, 传递给组件的**参数值默认是字符串**, 如要**传递变量**, 要**使用v-bind(也就是:)**

## 组件通信

这里主要讲父子组件的通信(例: app与子组件的通信)

+ 可以通过props传参的方式进行组件通信, 如前面所示
+ 通过`$emit()`发送自定义事件进行通信.

```javascript
// vm.js

let app = new Vue({
    el: "#app",
    data: {
        postFontSize: 1,  // style控制的fontSize
        posts: [
            {id:1, title:"hello", content:"word"},
            {id:2, title:"hello2", content:"word2"},
            {id:3, title:"hello3", content:"word3"},
        ]
    },
    template:  // blog-post组件, 设置了字体大小, 监听自定义事件enlarge-text
    `
    <div>
        <blog-post v-for="post in posts" :key="post.id" :title="post.title" :content="post.content" :style="{fontSize:postFontSize+'em'}" v-on:enlarge-text="postFontSize+=$event"></blog-post>
        <etest></etest>
    </div>
    `
});

// ==================================================
// cmp2.js   组件
export default Vue.component("blog-post",{
    props: ["title",'content'],
    template:
    `
    <div>
        <div>title: {{title}}</div>
        <p>{{content}}</p>
        <button @click="$emit('enlarge-text',0.2)">enlarge the fontsize</button>
    </div>
    `
})
```

子组件通过`$emit(事件,参数)`将事件发送给父级, 父级通过`$event`获取参数.



## 动态组件

在不同的组件之间进行切换是很常用的, 通过Vue的`<component>`元素配合`is`特性实现

```javascript
let app = new Vue({
    el: "#app",
    data: {
        state: ""
    },
    template:
    `
    <div>
        <button @click="state='etest'">etest</button>
        <button @click="state='blog-post'">blog</button>
        <component :is="state"></component>
    </div>
    `
});
```

通过改变`is`的值(注意使用v-bind), 达到动态切换组件

## 插槽slot

### 基本使用

如果我想在组件的标签内部写内容, 例如父级`<blog-post>写点内容呗</blog-post>`, 此时在子组件中, 就需要准备一个`<slot></slot>`来接受父级的"写点内容呗"

```javascript
// vm.js  父级
// ...
template:`<div><blog-post>插槽内容</blog-post></div>`


// cmp2.js  组件
// ...
template: `<div><slot></slot></div>`
// 此时 '插槽内容'会替换<slot></slot>渲染出来
```

同时, 插槽还可以有默认值`<slot>默认内容</slot>`, 如果没有向插槽传递内容, 此时, 插槽会显示默认内容.

### 具名插槽

在组件了可以有多个插槽, 分别具有不同name以区分. 父级传递插槽内容时, 使用`<template v-slot:插槽名>插槽内容</template>`, 组件内的插槽使用`<slot name="插槽名称"></slot>`接受内容

```javascript
// vm.js 父级
template:
`<blog-post>
    <template v-slot:header>这里是头部插槽</template>
    <!--默认插槽可以不写template-->
    <template>这里是没有名字的默认插槽</template>  
</blog-post>`
 
//=====================================
// cmp2.js  组件

template:
`<div>
    <slot name="header"></slot><br>
    <slot></slot>
</div>`
```

### 作用域插槽

例如element-ui需要自定义渲染时需要使用作用域插槽

```vue
<div id="app">
    <child>
        <!--必须使用template占位符，并且加上slot-scope-->
        <template slot-scope='scope'><li>{{scope.item}}</li></template>
    </child>
</div>

<script>
// 创建一个组件
Vue.component("child",{
    data(){
        return {
            list: [1,2,3,4,5]
        }
    },
    // 组件里面使用slot，通过:item(这个名字自定义)接收插槽scope的属性item
    template: `<ul><slot v-for="item in list" :item="item"></slot></ul>`
})

var app = new Vue({
  el: '#app',
  component: ['child'],
  data() {
    return {
    }
  }, 
  methods: {
  }
});
</script>
```

组件通过slot标签来让外部设置显示样式，通过`:item`（名字可自定义）传递出显示需要的数据(属性)，父级(外部)通过`<template slot-scope="scope">`来接受这个数据`{{scope.item}}`，然后在内部可以自定义显示的样式，比如通过`<li>`

# Vue动画

## 通过class样式类实现动画

Vue的动画定义了进场动画(Enter)和离开动画(leave), 这两组动画组合成一整场动画.

![整场动画](https://cn.vuejs.org/images/transition.png)

总共有6个类来定义动画

1. `v-enter`：定义进入过渡的开始状态。在元素被插入之前生效，在元素被插入之后的下一帧移除。(**进场动画开始前的状态**)
2. `v-enter-active`：定义进入过渡生效时的状态。在整个进入过渡的阶段中应用，在元素被插入之前生效，在过渡/动画完成之后移除。**这个类可以被用来定义进入过渡的过程时间，延迟和曲线函数。**
3. `v-enter-to`: **2.1.8版及以上** 定义**进入过渡的结束状态**。在元素被插入之后下一帧生效 (与此同时 `v-enter` 被移除)，在过渡/动画完成之后移除。
4. `v-leave`:  定义离开过渡的开始状态。在离开过渡被触发时立刻生效，下一帧被移除。(**离开动画开始前**)
5. `v-leave-active`：定义离开过渡生效时的状态。在整个离开过渡的阶段中应用，在离开过渡被触发时立刻生效，在过渡/动画完成之后移除。**这个类可以被用来定义离开过渡的过程时间，延迟和曲线函数。**
6. `v-leave-to`: **2.1.8版及以上** 定义**离开过渡的结束状态**。在离开过渡被触发之后下一帧生效 (与此同时 `v-leave` 被删除)，在过渡/动画完成之后移除。

```html
    <link href="https://cdn.bootcss.com/animate.css/3.5.2/animate.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <style>
        .v-enter, .v-leave-to {
            opacity: 0;
            transform: translateX(150px);
        }

        .v-enter-active, .v-leave-active {
            -webkit-transition: all 0.8s ease;
            -moz-transition: all 0.8s ease;
            -ms-transition: all 0.8s ease;
            -o-transition: all 0.8s ease;
            transition: all 0.8s ease;
        }

        .cus-enter, .cus-leave-to {
            transform: translateY(150px);
            opacity: 0;
        }

        .cus-enter-active, .cus-leave-active {
            -webkit-transition: all 0.8s ease;
            -moz-transition: all 0.8s ease;
            -ms-transition: all 0.8s ease;
            -o-transition: all 0.8s ease;
            transition: all 0.8s ease;
        }
    </style>
<!---->
<body>
<div id="app">
    <button @click="flag=!flag">切换</button>
    <transition>
        <h3 v-if="flag">标题</h3>
    </transition>

    <!--  自定义动画样式类名前缀 name: 样式里面写cus-enter...  -->
    <transition name="cus">
        <h3 v-if="flag">这个是自定义的</h3>
    </transition>

    <!--  使用animate.css  -->
    <transition enter-active-class="rotateIn" leave-active-class="rotateOut" :duration="{enter:800, leave:800}">
        <div v-if="flag" class="animated">第三方样式</div>
    </transition>
</div>
<script>
    let vm = new Vue({
        el: '#app',
        data: {
            flag: false
        }
    });
</script>
```

animate.css: https://daneden.github.io/animate.css/



**在2.2.0**之后版本, 可以显式的定义动画的时长:

```html
<transition :duration="1000">...</transition>

<!--定义进场和离开的时长-->
<transition :duration="{ enter: 500, leave: 800 }">...</transition>
```



## 使用animate.css完成动画

```html
<div id="app">
    <transition enter-active-class="animated swing" 
    leave-active-class="animated fadeOutDown"
    appear-active-class="animated fadeIn" appear>
    <child v-if="isShow">
        <template slot-scope='scope'><li>{{scope.item}}</li></template>
    </child>
    </transition>
    <button @click="ClickMe">Change</button>
</div>
```

通过指定: `enter-active-class:animated [预设名称]`和`leave-active-class`来设置enter和leave动画

元素初始渲染(第一次出现)的动画可以使用`appear-active-class="animated fadeIn"`和 `appear`属性指定



## 同时使用过渡和动画

即同时使用keyframe animation和transitioin

```html
<transition
    name="hello"
    enter-active-class="animated swing hello-enter-active" 
    leave-active-class="animated fadeOutDown"
    appear-active-class="animated fadeIn" appear>
</transition>
```

在enter-active-class后面再跟上你自定义的css transition动画名称



## JS钩子函数实现动画

除了使用css类来实现动画, 还可以通过js钩子函数来完成动画:

```html
<transition
  v-on:before-enter="beforeEnter"
  v-on:enter="enter"
  v-on:after-enter="afterEnter"
  v-on:enter-cancelled="enterCancelled"

  v-on:before-leave="beforeLeave"
  v-on:leave="leave"
  v-on:after-leave="afterLeave"
  v-on:leave-cancelled="leaveCancelled"
>
  <!-- ... -->
</transition>
```

```javascript
methods: {
    // 动画钩子函数的第一个参数el表示要执行动画的元素, 是个原生JS DOM对象那个
    beforeEnter(el) {
        el.style.transform = "translate(0,0)";  // 设置动画开始之前的状态
    },
    enter(el, done) {  // done表示动画完成的回调, 必须写上.
        el.offsetWidth;
        el.style.transition = "all 1s ease";
        el.style.transform = "translate(150px, 450px)";
        done();   // 动画完成的回调
    },
    afterEnter(el) {   // 动画结束的操作
        //...
    }
     enterCancelled: function (el) {
    // ...
  },

      // --------
      // 离开时
      // --------

      beforeLeave: function (el) {
        // ...
      },
      // 当与 CSS 结合使用时
      // 回调函数 done 是可选的
      leave: function (el, done) {
        // ...
        done()
      },
      afterLeave: function (el) {
        // ...
      },
      // leaveCancelled 只用于 v-show 中
      leaveCancelled: function (el) {
        // ...
      }
}
```

还可以配合**Velocity.js**完成钩子函数实现动画.(目前测试版本velocity.js为1.3.2可以完美使用)

```html
<script src="https://cdn.bootcss.com/velocity/1.3.2/velocity.min.js"></script>
```

所有的设置项:

```javascript
    $element.velocity({
        width: "500px",        // 动画属性 宽度到 "500px" 的动画
        property2: value2      // 属性示例
    }, {
        /* Velocity 动画配置项的默认值 */
        duration: 400,         // 动画执行时间
        easing: "swing",       // 缓动效果
        queue: "",             // 队列
        begin: undefined,      // 动画开始时的回调函数
        progress: undefined,   // 动画执行中的回调函数（该函数会随着动画执行被不断触发）
        complete: undefined,   // 动画结束时的回调函数
        display: undefined,    // 动画结束时设置元素的 css display 属性
        visibility: undefined, // 动画结束时设置元素的 css visibility 属性
        loop: false,           // 循环
        delay: false,          // 延迟
        mobileHA: true         // 移动端硬件加速（默认开启）
    });
```

配合Vue完成

```javascript
beforeEnter: function (el) {
                el.style.opacity = 0;
                el.style.transformOrigin = 'left';
                Velocity(el, {rotateZ:'-180deg'});
            },
enter: function (el, done) {
    Velocity(el, {opacity: 1, fontSize: '1.4em', rotateZ:'0deg'}, {duration: 300, easing:'ease'});
    Velocity(el, {fontSize: '1em'}, {complete: done});
},
leave: function (el, done) {
    Velocity(el, {translateX: '15px', rotateZ: '50deg'}, {duration: 600});
    Velocity(el, {rotateZ: '100deg'}, {loop: 2});
    Velocity(el, {
        rotateZ: '45deg',
        translateY: '30px',
        translateX: '30px',
        opacity: 0
    }, {complete: done})
}
```

## 初始状态的渲染appear

主要封装在这三个类里面, 用于渲染初始状态的动画

```html
<transition appear appear-class="custom-appear-class"
                appear-to-class="custom-appear-to-class"
                appear-active-class="custom-appear-active-class">
```

## 多个元素的动画渲染

```html
<!--定义好过渡类-->

<transition mode="out-in">
    <button v-if="!flag" @click="flag=!flag" key="on">on</button>
    <button v-else key="off" @click="flag=!flag">off</button>
</transition>

<!--....................-->

<script>
    let vm = new Vue({
        el: '#app',
        data: {
            flag: false
        }
    })
</script>
```

transition有一个属性为mode:

+ in-out：新元素先进行过渡，完成之后当前元素过渡离开。
+ out-in：当前元素先进行过渡，完成之后新元素过渡进入。(**常用**)



## 多组件的动画

```html
<!--定义好过渡类-->

<button @click="toggleView()">切换</button>
<hr>
<transition mode="out-in">
    <component :is="view"></component>
</transition>
```

js代码:

```javascript
<script>
    let cmp1 = Vue.component('v-a',{
        template: `<button>1</button>`
    });
    let cmp2 = Vue.component('v-b',{
        template: `<button>2</button>`
    });
    let cmp3 = Vue.component('v-c',{
        template: `<button>3</button>`
    });
    let vm = new Vue({
        el: '#app',
        data: {
            view: 'v-a'
        },
        methods: {
            // 切换view指向的component, 即切换:is
            toggleView(){
                if(this.view==='v-a'){
                    this.view = 'v-b';
                }else if(this.view=='v-b'){
                    this.view = 'v-c';
                }else{
                    this.view = 'v-a';
                }
            }
        }
    })
</script>
```



## 列表动画



```html
<div id="app">
    <button @click="insertRandom()">insert</button>
    <button @click="removeList()">remove</button>
    <button @click="shuffleList()">shuffle</button>
    <button @click="sortList()">sort</button>
    <!--自定义动画类名前缀  list-->
    <transition-group name="list" tag="ul" mode="out-in">
        <li v-for="item in list" :key="item">{{item}}</li>
    </transition-group>
</div>
```

```css
ul {
    list-style: none;
}

li {
    display: inline-block;
    width: 50px;
    height: 50px;
    text-align: center;
    line-height: 50px;
}
/* 动画 */
.list-enter {
    opacity: 0;
    transform: translateY(20px);
}
.list-enter-to {
    opacity: 1;
    transform: translateY(0);
}
.list-leave-to {
    opacity: 0;
    transform: translateY(-20px);
}
.list-enter-active, .list-leave-active{
    -webkit-transition: all .6s ease;
    -moz-transition: all .6s ease;
    -ms-transition: all .6s ease;
    -o-transition: all .6s ease;
    transition: all .6s ease;
}
/* 因为只用自定义前缀, v-move改为list-move */
.list-move {
    -webkit-transition: transform  .6s;
    -moz-transition: transform  .6s;
    -ms-transition: transform  .6s;
    -o-transition: transform  .6s;
    transition: transform  .6s;
}
.list-leave-active{
    position:absolute;  /*在元素离开动画时, 防止其他元素瞬移到离开元素的位置*/
}
```

`<transition-group>` 组件还有一个特殊之处。不仅可以进入和离开动画，还可以改变定位。要使用这个新功能只需了解新增的  **v-move  特性**，它会在元素的改变定位的过程中应用。

后面js的函数都简单, 不在啰嗦.



# Vue2.x与vue-cli

之前介绍的写法基本是基于vue1.x的写法, 在2.x时代, 项目的结构和代码的格式变得更加规范, 也诞生了Vue组件的文件格式(.vue, 其实也是编译成.js), 使用vue-cli创建vue项目的过程:

`vue create [ProjectName]`然后基本就是下一步下一步

最终的目录结构

![8581b7f698b06b168eed1e6d7a96b679.png](https://www.z4a.net/images/2019/06/04/8581b7f698b06b168eed1e6d7a96b679.png)

此时在build下也有webpack的多个配置文件, base里面已经修正了之前引入vue的写法, 并且增加了src的目录路径的引用

```javascript
resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
    }
},
```

之后引入Vue的写法只需要`import Vue from 'vue'`即可,  引入组件则是: `import XXX from '@/components/XXX'`

一个Vue组件的代码结构变成了:

```vue
<template>
  <div>
    这里是template的区域
  </div>
</template>

<script>
// 这是根
export default {
  name: 'App',
  data () {
    return {a: 12}
  },
  components: {}  // import过来的组件必须在这里注册.
}
</script>

<style>
/*这里面可以写当前的template的样式*/
</style>

```



# VueX

Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式**。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化.

简而言之, 集中管理所有数据(data), 不用像之前那样子级找父级, 父级找子级那么恶心.

解决问题:

1. 数据跨组件共享
2. 防止数据意外修改
3. 调试方便

![vuex](https://vuex.vuejs.org/vuex.png)

vuex抽象出了3个概念:

+ State: 数据源(也就是所有的数据), 默认没有办法直接更改, 只能通过Mutations更改
+ Mutations: 改变数据的操作, 可以通过Vue的devtools插件记录操作历史, 一般一个mutation改变一项数据
+ Actions: 理解为改变数据的方法(函数). 对mutations进行一次封装, 组合多个mutations改变多项数据

## State

存储全局状态。

```javascript
const store = new Vuex.Store({
    state: {
        count: 12,
        title: 'hello world'
    }
});
```

要读取store中的state，常用的做法是使用组件中的`computed`属性

```javascript
const cmp1 = {
    computed: {
        count(){
            return this.$store.state.count
        },
        title(){
            return this.$store.state.title
        }
    }
}
```

使用***mapState***函数可以减少代码量，偷偷懒。

```javascript
computed: mapState({
    // 箭头函数可使代码更简练
    count: state => state.count,

    // 传字符串参数 'count' 等同于 `state => state.count`
    countAlias: 'count',

    // 为了能够使用 `this` 获取局部状态，必须使用常规函数
    countPlusLocalState (state) {
      return state.count + this.localCount
    }
})
```

如果还有其他不是Vuex的computed属性，可以使用扩展运算符将mapState展开

```javascript
computed: {
    ...Vuex.mapState({
    count:"count",
    title:"title"
    })
}
```

## Getter

类似computed是对Vue中的data进行计算封装，Getter也是对Vuex中state存储数据的计算封装.使用`getters`属性定义

```javascript
const store = new Vuex.Store({
  state: {
    todos: [
      { id: 1, text: '...', done: true },
      { id: 2, text: '...', done: false }
    ]
  },
  getters: {
    doneTodos: state => {
      return state.todos.filter(todo => todo.done)
    }
  }
})
```

同时，*Getter*的还可以接受其他*getter*作为第二个参数：

```javascript
getters: {
  // ...
  doneTodosCount: (state, getters) => {
    return getters.doneTodos.length
  }
}
```

通过`store.getters`来访问所有的*getter*，同时*getter*还可以接受自定义参数：

```javascript
getters: {
  // ...
  getTodoById: (state) => (id) => {
    return state.todos.find(todo => todo.id === id)
  }
}

//............
store.getters.getTodoById(2) // -> { id: 2, text: '...', done: false }
```

同样的，也可以通过***mapGetters***来偷懒，依旧可以在组件的computed属性中获取getter

```javascript
computed: {
    // 使用对象展开运算符将 getter 混入 computed 对象中
    ...mapGetters([
      'doneTodosCount',
      'anotherGetter',
      // ...
    ])
}
```

## Mutation

更改 Vuex 的 store 中的状态的唯一方法是提交 mutation

```javascript
 const store = new Vuex.Store({
  state: {
    count: 1
  },
  mutations: {
    increment (state) {
      // 变更状态
      state.count++
    }
  }
})
```

提交mutation：`store.commit('increment')`

同时在提交时可以传入额外的参数，即*mutation*的***payload***，可以是一个参数，也可以是一个对象。

```javascript
// 普通参数
mutations: {
  increment (state, n) {
    state.count += n
  }
}

//........
store.commit('increment', 10)
```

```javascript
// 对象
mutations: {
  increment (state, payload) {
    state.count += payload.amount
  }
}

// 提交
store.commit('increment', {
  amount: 10
})
```

**mutation**必须是**同步函数**。

## Action

- Action 提交的是 mutation，而不是直接变更状态。
- Action 可以包含任意异步操作。(比如延时setTimeout)

基本示例：

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  actions: {
    increment (context) {
      context.commit('increment')
    }
  }
})
```

注意*action*函数接受的参数为与 *store* 实例具有相同方法和属性的 *context* 对象，包含以下属性:

```javascript
{
  state,      // 等同于 `store.state`，若在模块中则为局部状态
  rootState,  // 等同于 `store.state`，只存在于模块中
  commit,     // 等同于 `store.commit`
  dispatch,   // 等同于 `store.dispatch`
  getters,    // 等同于 `store.getters`
  rootGetters // 等同于 `store.getters`，只存在于模块中
}

// 可以是使用参数解构来传递, 如：increment({commit})
```

*Action*通过`store.dispatch`触发, 同样支持传递*payload*， 同样具有mapAction方法：将`store.dispatch(...)`映射为*methods*

```javascript
methods: {
    ...mapActions([
      'increment', // 将 `this.increment()` 映射为 `this.$store.dispatch('increment')`

      // `mapActions` 也支持载荷：
      'incrementBy' // 将 `this.incrementBy(amount)` 映射为 `this.$store.dispatch('incrementBy', amount)`
    ]),
    ...mapActions({
      add: 'increment' // 将 `this.add()` 映射为 `this.$store.dispatch('increment')`
    })
}
```

*Action*是支持异步的，其返回值是一个*Promise*对象，因此可以使用回调来设置异步操作完成后的操作。

```javascript
store.dispatch('actionA').then(() => {
  // ...
})
```

同样也可以支持*async*/*await*操作。

```javascript
// 假设 getData() 和 getOtherData() 返回的是 Promise

actions: {
  async actionA ({ commit }) {
    commit('gotData', await getData())
  },
  async actionB ({ dispatch, commit }) {
    await dispatch('actionA') // 等待 actionA 完成
    commit('gotOtherData', await getOtherData())
  }
}
```



## Module

用途： 将store分割成多个模块(*module*)，每个模块都有自己的*state, mutation, action, getter*，甚至嵌套子模块。

### 基本使用

```javascript
const moduleA = {
  state: { ... },
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}

const moduleB = {
  state: { ... },
  mutations: { ... },
  actions: { ... }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})

store.state.a // -> moduleA 的状态
store.state.b // -> moduleB 的状态
```

模块的state

```javascript
const moduleA = {
  state: { count: 0 },
  mutations: {
    increment (state) {
      // 这里的 `state` 对象是模块的局部状态
      state.count++
    }
  },

  getters: {
    doubleCount (state) {
      return state.count * 2
    }
  }
}
```

模块的getter,会接受以下参数

```javascript
state,       // 如果在模块中定义则为模块的局部状态
getters,     // 等同于 store.getters
rootState    // 等同于 store.state
rootGetters  // 所有 getters
```

模块的mutation接受的state是局部状态，且只能修改模块自身的state，无法访问root的state

模块的action可以接受如下参数：

```javascript
{
  state,      // 等同于 `store.state`，若在模块中则为局部状态
  rootState,  // 等同于 `store.state`，只存在于模块中
  commit,     // 等同于 `store.commit`
  dispatch,   // 等同于 `store.dispatch`
  getters,    // 等同于 `store.getters`
  rootGetters // 等同于 `store.getters`，只存在于模块中
}
```

### 命名空间

默认情况下，模块内部的 action、mutation 和 getter 是注册在**全局命名空间**的——这样使得多个模块能够对同一 mutation 或 action 作出响应。

```javascript
const store = new Vuex.Store({
  modules: {
    account: {
      namespaced: true,   // 使用命名空间

      // 模块内容（module assets）
      state: { ... }, // 模块内的状态已经是嵌套的了，使用 `namespaced` 属性不会对其产生影响
      getters: {
        isAdmin () { ... } // -> getters['account/isAdmin']
      },
      actions: {
        login () { ... } // -> dispatch('account/login')
      },
      mutations: {
        login () { ... } // -> commit('account/login')
      },

      // 嵌套模块
      modules: {
        // 继承父模块的命名空间
        myPage: {
          state: { ... },
          getters: {
            profile () { ... } // -> getters['account/profile']
          }
        },

        // 进一步嵌套命名空间
        posts: {
          namespaced: true,

          state: { ... },
          getters: {
            popular () { ... } // -> getters['account/posts/popular']
          }
        }
      }
    }
  }
})
```

### 在带命名空间的模块内访问全局内容（Global Assets）

若需要在全局命名空间内分发 action 或提交 mutation，将 `{ root: true }` 作为第三参数传给 `dispatch` 或 `commit` 即可(第二个参数是*payload*)。

```javascript
modules: {
  foo: {
    namespaced: true,
    // ...
    actions: {
      // 在这个模块中， dispatch 和 commit 也被局部化了
      // 他们可以接受 `root` 属性以访问根 dispatch 或 commit
      someAction ({ dispatch, commit, getters, rootGetters }) {
        getters.someGetter // -> 'foo/someGetter'
        rootGetters.someGetter // -> 'someGetter'

        dispatch('someOtherAction') // -> 'foo/someOtherAction'
        dispatch('someOtherAction', null, { root: true }) // -> 'someOtherAction'

        commit('someMutation') // -> 'foo/someMutation'
        commit('someMutation', null, { root: true }) // -> 'someMutation'
      },
      someOtherAction (ctx, payload) { ... }
    }
  }
}
```

### 在带命名空间的模块注册全局 action

```javascript
{
  actions: {
    someOtherAction ({dispatch}) {
      dispatch('someAction')
    }
  },
  modules: {
    foo: {
      namespaced: true,

      actions: {
        someAction: {
          root: true,
          handler (namespacedContext, payload) { ... } // -> 'someAction'
        }
      }
    }
  }
}
```

### 带命名空间的绑定函数

方式一：

```javascript
computed: {
  ...mapState('some/nested/module', {
    a: state => state.a,
    b: state => state.b
  })
},
methods: {
  ...mapActions('some/nested/module', [
    'foo', // -> this.foo()
    'bar' // -> this.bar()
  ])
}
```

方式二：

```javascript
import { createNamespacedHelpers } from 'vuex'

const { mapState, mapActions } = createNamespacedHelpers('some/nested/module')

export default {
  computed: {
    // 在 `some/nested/module` 中查找
    ...mapState({
      a: state => state.a,
      b: state => state.b
    })
  },
  methods: {
    // 在 `some/nested/module` 中查找
    ...mapActions([
      'foo',
      'bar'
    ])
  }
}
```




































